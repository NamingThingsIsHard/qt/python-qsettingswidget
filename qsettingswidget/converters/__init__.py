# python-qsettingswidget
# Copyright (C) 2019  LoveIsGrief
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# TODO: Maybe memoize this
def get_converter(converters, widget, strict=True):
    """
    Helper to find a converter for the widget

    :type converters: list[converters.converter.Converter]
    :type widget: PyQt5.QtWidgets.QWidget
    :param strict: Throw an exception when no converter can be found
    :type strict: bool
    :return:
    :rtype: converters.converter.Converter
    """
    converter = next((converter
                      for converter in converters
                      if converter.can_convert(widget)
                      ), None)
    if strict and converter is None:
        raise ValueError("No converter found", widget)
    return converter
